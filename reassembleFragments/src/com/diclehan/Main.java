package com.diclehan;

import java.io.BufferedReader;
import java.io.FileReader;

public class Main {

	public static void main(String[] args) {

		Fragment d = new Fragment();
		try (BufferedReader in = new BufferedReader(new FileReader(args[0]))) { // input file path
			String fragmentProblem;
			while ((fragmentProblem = in.readLine()) != null) {
				System.out.println(d.reassemble(fragmentProblem));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
