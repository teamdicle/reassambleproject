package com.diclehan;

import java.util.ArrayList;

public class Fragment {
	// The priority function is used to define which fragment should come first.
	// Ex: "iquam quaerat vol" and "aerat voluptatem." are inputs. So first fragment
	// should come first and second fragment should
	// connect to the first one.
	// index is used to define at least how much character should match to be merged
	// together
	private String priority(String fragment1, String fragment2, int index) {
		return (fragment1.contains(fragment2.substring(0, index))) ? fragment1 : fragment2;

	}

	// The mergePart function is used to find which sub part of the fragments are
	// common
	// Ex: Ex: "iquam quaerat vol" and "aerat voluptatem." are inputs.
	// So "aerat vol" is the common part of two fragments.
	// Index is used to find at least how much characters are common.
	private String mergePart(String fragment1, String fragment2, int index) {
		String exchange = "", mergeString = "";
		exchange = priority(fragment1, fragment2, index);
		// exchange is a temporary fragment which holds the priority fragment.
		if (exchange.equals(fragment2)) {
			// I assume that fragments1 will be coming always first, second will be
			// connected next to first one.
			exchange = fragment1;
			fragment1 = fragment2;
			fragment2 = exchange;
		}

		while (fragment1.contains(fragment2.substring(0, index))) {
			mergeString = fragment2.substring(0, index);
			if (index == fragment2.length()) {
				// index is keeping the number of the common parts.
				// Ex: 	"iquam quaerat vol" and "aerat voluptatem." are inputs.
				// "aerat vol" is common part. So till index will be 8 while loop will find the
				// common part.
				break;
			} else
				index++;
		}

		return (fragment1.endsWith(mergeString) || (mergeString.equals(fragment2))) ? mergeString : "";
		// Finally common part of fragments will be returned as result.
	}

	public String reassemble(String fragmentCol) {
		// Whole merge process is implemented for the given fragments
		String fragment1, fragment2, commonPart = "";
		int mergeIndex;
		String mergedFragment = "";
		String[] items = fragmentCol.split(";");
		ArrayList<String> fragments = new ArrayList<>();
		for (String fragment : items) { // All fragment pieces are in the fragment list
			fragments.add(fragment);
		}
		do {
			fragment1 = fragments.get(0).toString();
			fragments.remove(fragment1);
			// I am getting first fragment into fragment list. And i am removing from the
			// list.
			// Otherwise the fragment can be compared itself.
			for (int i = 0; i < fragments.size(); i++) {
				// First fragment will be compared with other fragments, if they have common
				// part.
				fragment2 = fragments.get(i).toString();
				// I am getting second fragments to be compared with the first one. Checking if
				// they have common part.
				commonPart = mergePart(fragment1, fragment2, 3);
				// mergeIndex is defines at least how much character needed to say they are
				// matching.
				// Two character is not enough to say they can be merge. However in the example
				// there are one fragment have to match with 2 length part.
				// This two character common part is an exception . So i assume that only one
				// fragment can match with two length common part.
				mergeIndex = 3;
				// Ex: "h lame sa" and "saint!" inputs have two letter common. but for other
				// examples if i assume 2 is enough
				// two different can be merged wrongly.
				// Ex: "olore magnam aliqua" and "iquam quaerat vol;" inputs have two letter common
				// but they should not match.
				if (commonPart == "" && fragments.size() <= 2) {
					// This if is for exceptional two letter common part fragments and i assume that
					// i can be happened only once.
					// So i left it as last.
					commonPart = mergePart(fragment1, fragment2, 2);
					mergeIndex = 2;
				}
				// if two fragments have common parts they needs to be merged together shown
				// below.
				if (commonPart != "") {
					if (priority(fragment1, fragment2, mergeIndex).equals(fragment1)) {
						mergedFragment = fragment1.concat(fragment2.replaceAll(commonPart, ""));
					} else {
						mergedFragment = fragment2.concat(fragment1.replaceAll(commonPart, ""));
					}
					// Merged fragment will be added to the list.
					// Comparison fragments will be deleted.
					fragments.add(mergedFragment);
					fragments.remove(fragment2);
					//fragments.remove(fragment1);
					break;
				}
				else // if fragments are not matching it should be added to the list again
				{ 
					fragments.add(fragment1);
				}
			} // for loop ends
		} while (fragments.size() != 1);
		// if in the end the least has at least 2 fragments that means they are not matching.
		return fragments.size()==1? fragments.get(0).toString() : "";
	}

}